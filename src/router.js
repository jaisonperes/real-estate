import Vue from 'vue'
import Router from 'vue-router'
import store from './store'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: () => import('./layouts/default'),
      children: [
        {
          path: '/',
          name: 'home',
          component: () => import('./pages/Home')
        },
        {
          path: '/imoveis',
          name: 'Imóveis',
          component: () => import('./pages/Properties')
        },
        {
          path: '/sobre',
          name: 'Sobre',
          component: () => import('./pages/About')
        },
        {
          path: '/cadastrar',
          name: 'Cadastrar',
          component: () => import('./pages/SignUp'),
        },
        {
          path: '/entrar',
          name: 'Entrar',
          component: () => import('./pages/SignIn')
        },
        {
          path: '/perfil',
          name: 'Perfil',
          component: () => import('./pages/Profile'),
          // Validate user logged
          beforeEnter (to, from, next) {
            if (store.getters.getUser === null || store.getters.getToken === null) {
              next('/')
            } else {
              next()
            }
          }
        },
        {
          path: '*',
          component: () => import('./pages/404')
        }
      ]
    }
  ]
})
