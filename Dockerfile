FROM node:9-alpine as base
ENV API="http://localhost:8000"
RUN mkdir /app
ADD . /app
ADD package.json app/package.json
WORKDIR /app
RUN npm i npm@latest yarn@latest -g
RUN npm install
LABEL enviroment="dev"
CMD ["yarn", "serve"]